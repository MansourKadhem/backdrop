import 'package:backdrop/backdrop.dart';
import 'package:flutter/material.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Backdrop Demo',
      home: BackdropScaffold(
        title: Text("Backdrop Example"),
        enableHeader: true,
        backLayer: Center(
          child: Text("Back Layer"),
        ),
        frontLayer: Center(
          child: Text("Front Layer"),
        ),
        frontLayerHeight: 333.0,
        enableAddingInactiveAreaToPanelWhenOpened:true,
        enableTapbacklayertohide:true,
        iconPosition: BackdropIconPosition.leading,
        headerHeight: 60.0,
        InactiveArea: new Text('HALLOLUIA'),
        actions: <Widget>[
          BackdropToggleButton(
            icon: AnimatedIcons.list_view,
          ),
        ],
      ),
    );
  }
}
