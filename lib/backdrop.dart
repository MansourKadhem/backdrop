library backdrop;

import 'dart:async';

import 'package:flutter/material.dart';

class Backdrop extends InheritedWidget {
  final _BackdropScaffoldState data;

  Backdrop({Key key, @required this.data, @required Widget child})
      : super(key: key, child: child);

  static _BackdropScaffoldState of(BuildContext context) =>
      (context.inheritFromWidgetOfExactType(Backdrop) as Backdrop).data;

  @override
  bool updateShouldNotify(Backdrop old) => true;
}

class BackdropScaffold extends StatefulWidget {
  final AnimationController controller;
  final Widget title;
  final Widget backLayer;
  final Widget frontLayer;
  final Widget InactiveArea;
  final double frontLayerHeight;
  final List<Widget> actions;
  final double headerHeight;
  final BorderRadius frontLayerBorderRadius;
  final BackdropIconPosition iconPosition;
  final bool enableHeader;
  final bool enableTapbacklayertohide;
  final bool enableAddingInactiveAreaToPanelWhenOpened;

  BackdropScaffold({
    this.controller,
    this.title,
    this.enableHeader,
    this.enableTapbacklayertohide = false,
    this.enableAddingInactiveAreaToPanelWhenOpened = false,
    this.backLayer,
    this.frontLayer,
    this.InactiveArea,
    this.frontLayerHeight,
    this.actions = const <Widget>[],
    this.headerHeight = 32.0,
    this.frontLayerBorderRadius = const BorderRadius.only(
      topLeft: Radius.circular(16.0),
      topRight: Radius.circular(16.0),
    ),
    this.iconPosition = BackdropIconPosition.leading,
  });

  @override
  _BackdropScaffoldState createState() => _BackdropScaffoldState();
}

class _BackdropScaffoldState extends State<BackdropScaffold>
    with SingleTickerProviderStateMixin {
  bool shouldDisposeController = false;
  AnimationController _controller;
  final scaffoldKey = GlobalKey<ScaffoldState>();
  dynamic size;

  AnimationController get controller => _controller;

  @override
  void initState() {
    super.initState();
    if (widget.controller == null) {
      shouldDisposeController = true;
      _controller = AnimationController(
          vsync: this, duration: Duration(milliseconds: 100), value: 0.0);
    } else {
      _controller = widget.controller;
    }
  }

  @override
  void dispose() {
    super.dispose();
    if (shouldDisposeController) {
      _controller.dispose();
    }
  }

  bool get isTopPanelVisible {
    final AnimationStatus status = controller.status;
    return status == AnimationStatus.completed ||
        status == AnimationStatus.forward;
  }

  bool get isBackPanelVisible {
    final AnimationStatus status = controller.status;
    return status == AnimationStatus.dismissed ||
        status == AnimationStatus.reverse;
  }

  void fling() {
    controller.fling(velocity: isTopPanelVisible ? -1.0 : 1.0);
  }

  void showBackLayer() {
    if (isTopPanelVisible) {
      controller.fling(velocity: -1.0);
    }
  }

  void showFrontLayer() {
    if (isBackPanelVisible) {
      controller.fling(velocity: 1.0);
    }
  }

  Animation<RelativeRect> getPanelAnimation(
      BuildContext context, BoxConstraints constraints, maximumheight) {
    final height = constraints.biggest.height;
    final backPanelHeight = height - widget.headerHeight;
    final frontPanelHeight = -backPanelHeight;

    return RelativeRectTween(
      begin: RelativeRect.fromLTRB(0.0, backPanelHeight, 0.0, frontPanelHeight),
      end: RelativeRect.fromLTRB(
          0.0, constraints.biggest.height - maximumheight, 0.0, 0.0),
    ).animate(CurvedAnimation(
      parent: controller,
      curve: Curves.linear,
    ));
  }

  Widget _buildInactiveLayer(BuildContext context, Widget InactiveArea) {
    return Offstage(
      offstage: isTopPanelVisible,
      child: GestureDetector(
        onVerticalDragEnd: (details) {
          print(details);
          if (details.velocity.pixelsPerSecond.dy < 0.0) {
            showFrontLayer();
          }
        },
        behavior: HitTestBehavior.opaque,
        child: SizedBox.expand(
          child: Container(
            child: InactiveArea,
          ),
        ),
      ),
    );
  }

  Widget _buildBackPanel({bool enableTapToHide = true}) {
    return new GestureDetector(
      child: Material(
        color: Theme.of(context).primaryColor,
        child: widget.backLayer,
      ),
      onTap: () {
        print("tapped on the backpanel");
        isTopPanelVisible ? enableTapToHide ? showBackLayer() : null : null;
      },
    );
  }

  Widget _buildFrontPanel(BuildContext context, Widget InactiveAreaWidget) {
    return new GestureDetector(
      child: Material(
        elevation: 12.0,
        borderRadius: widget.frontLayerBorderRadius,
        child: Stack(
          children: <Widget>[
            new Column(
              children: <Widget>[
                widget.enableAddingInactiveAreaToPanelWhenOpened
                    ? isTopPanelVisible ? InactiveAreaWidget : new Container()
                    : new Container(),
                isTopPanelVisible ? widget.frontLayer : new Container(),
              ],
            ),
            _buildInactiveLayer(context, InactiveAreaWidget),
          ],
        ),
      ),
      onVerticalDragEnd: (details) {
        print(details);
        if (details.velocity.pixelsPerSecond.dy > 0.0) {
          showBackLayer();
        }
      },
    );
  }

  void onTapDown(BuildContext context, TapDownDetails details, key) {
    final RenderBox box = context.findRenderObject();
    final Offset localOffset = box.globalToLocal(details.globalPosition);
    final RenderBox containerBox = key.currentContext.findRenderObject();
    final Offset containerOffset = containerBox.localToGlobal(localOffset);
    final onTap = containerBox.paintBounds.contains(containerOffset);
    if (onTap) {
      showBackLayer();
    }
  }

  Future<bool> _willPopCallback(BuildContext context) async {
    if (isBackPanelVisible) {
      showFrontLayer();
      return null;
    }
    return true;
  }

  Widget _buildBody(BuildContext context) {
    return WillPopScope(
      onWillPop: () => _willPopCallback(context),
      child: Scaffold(
        key: scaffoldKey,
        appBar: widget.enableHeader == true
            ? AppBar(
                title: widget.title,
                actions: widget.iconPosition == BackdropIconPosition.action
                    ? <Widget>[BackdropToggleButton()] + widget.actions
                    : widget.actions,
                elevation: 0.0,
                leading: widget.iconPosition == BackdropIconPosition.leading
                    ? BackdropToggleButton()
                    : null,
              )
            : null,
        body: LayoutBuilder(
          builder: (context, constraints) {
            return Container(
              child: Stack(
                children: <Widget>[
                  _buildBackPanel(
                      enableTapToHide: widget.enableTapbacklayertohide),
                  isTopPanelVisible ? new GestureDetector(
                    child: new Container(
                      height: size.height,
                      width: size.width,
                      child: new Container(),
                    ),
                    behavior: HitTestBehavior.translucent,
                    onTap: () {
                      isTopPanelVisible ? widget.enableTapbacklayertohide ? showBackLayer() : null : null;
                    },
                  ):new Container(),
                  PositionedTransition(
                    rect: getPanelAnimation(
                        context, constraints, widget.frontLayerHeight),
                    child: _buildFrontPanel(context, widget.InactiveArea),
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Backdrop(
      data: this,
      child: Builder(
        builder: (context) => _buildBody(context),
      ),
    );
  }
}

class BackdropToggleButton extends StatelessWidget {
  final AnimatedIconData icon;

  const BackdropToggleButton({
    this.icon = AnimatedIcons.close_menu,
  });

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: AnimatedIcon(
        icon: icon,
        progress: Backdrop.of(context).controller.view,
      ),
      onPressed: () => Backdrop.of(context).fling(),
    );
  }
}

enum BackdropIconPosition { none, leading, action }
